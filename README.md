# Plain text notation for proofreading and review

![text, correction, comment](tcc.svg)

This short document shows how to use brackets [ ] to advise lossless changes within a plain text document.

## In short

When the proofreader wants to advise a change, she or he will use a the following thee boxes structure:

    [1][2][3]
    
    1: Original text
    2: Text to replace with
    3: Comment

2, and 3 are optional.

## Addition

    Are big corporations destroying the planet[][?]

The corrector suggests to add a “?”.

## Deletion

    Big corporations are [sometimes] destroying the planet.

Delete “sometimes”.

    Big corporations are [sometimes][] destroying the planet.

Or delete “dear” by replacing it with nothing.

## Replacement

    Big corporations are destroying [the][our] planet.

Replace “the” by “our”.

## Comment

    Big corporations[][][This word could be changed] are destroying the planet.

Nothing changed, just a comment.

    Big [corporations][companies][This word could be changed] are destroying the planet.

Replace “corporations” by “companies” as the comment advises.

## Inspiration and resources

Inspired by [Collaboration Made Simple with Bracket Notation](https://pairlist6.pair.net/pipermail/markdown-discuss/2008-February/000962.html)

## License

This text is under [WTFPL](http://www.wtfpl.net/)
